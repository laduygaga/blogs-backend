package router

import (
	"html"
	"html/template"
	"net/http"
	"os"

	"github.com/PacodiazDG/Backend-blog/api/v1/admin"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func index(c *gin.Context) {
	listOfUsers, err := admin.ListUsers(0, "")
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"Status": err.Error()})
		return
	}

	tmpl := template.Must(template.ParseFiles("templates/index.html"))
	err = tmpl.Execute(c.Writer, listOfUsers)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"Status": err.Error()})
		return
	}
}
func page(c *gin.Context) {
	PostID, err := primitive.ObjectIDFromHex(c.Query("id"))
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"Stauts": "Id not valid"})
		return
	}
	result, err := blogs.Conf.GetMetaPost(PostID)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"Stauts": err.Error()})
		return
	}
	if !result.Visible {
		c.HTML(http.StatusOK, "index.html", gin.H{
			"title":         "",
			"description":   "",
			"site_name":     "",
			"ogimage":       "",
			"gverification": "",
		})
		return
	}
	c.HTML(http.StatusOK, "index.html", gin.H{
		"title":         html.EscapeString(result.Title),
		"description":   html.EscapeString(result.Description),
		"site_name":     html.EscapeString(os.Getenv("SiteMetaTitle")),
		"ogimage":       html.EscapeString(result.Imagen),
		"gverification": html.EscapeString(os.Getenv("GoogleSite_Verification")),
	})
}
func P404(c *gin.Context) {
	c.HTML(http.StatusNotFound, "index.html", gin.H{
		"title":       "Page not found",
		"description": "Page not found",
		"site_name":   html.EscapeString(os.Getenv("SiteMetaTitle")),
	})
}
func PageManagement(router *gin.Engine) {

	if os.Getenv("Pages") != "true" {
		router.GET("/", func(ctx *gin.Context) {
			ctx.AbortWithStatusJSON(http.StatusOK, gin.H{"Status": "Ok"})
		})
		return
	}
	router.GET("/", index)
	router.GET("/404", P404)
	router.GET("/Pages", page)
	router.Static("/static/", "./static")
	router.NoRoute(index)

}
